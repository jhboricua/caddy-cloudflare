# Define caddy image tag to use
ARG IMAGE_TAG=latest

# Use the argument in the FROM instruction
FROM docker.io/caddy:${IMAGE_TAG}-builder AS builder

# Build the caddy binary with the cloudflare plugin
RUN xcaddy build \
  --with github.com/caddy-dns/cloudflare

# Copy the caddy binary to the caddy image
FROM docker.io/caddy:${IMAGE_TAG}
COPY --from=builder /usr/bin/caddy /usr/bin/caddy
